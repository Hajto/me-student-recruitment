name := "ATM_recruiting_project"

version := "1.0"

val scalaVersionRef = "2.12.0"
val organizationRef = "com.atm"

scalaVersion := scalaVersionRef

fork := true // for test config to work

lazy val commonSettings = Seq(
  version := version.value,
  organization := organizationRef,
  scalaVersion := scalaVersionRef,
  test in assembly := {}
)

lazy val app = (project in file("app")).
  settings(commonSettings: _*).
  settings(
    mainClass in assembly := Some(organizationRef + ".Main")
  )

javaOptions in Test += "-Dconfig.resource=test.conf"

resolvers +=
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases"
resolvers +=
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.3",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.3" % Test
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.9",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.9" % Test,
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.9",
  "com.typesafe.akka" %% "akka-stream" % "2.5.3",
  "com.typesafe.akka" %% "akka-persistence" % "2.5.3"
)

libraryDependencies ++= Seq(
  "com.softwaremill.akka-http-session" %% "core" % "0.5.0",
  "com.softwaremill.akka-http-session" %% "jwt" % "0.5.0"
)

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"

libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.3"

libraryDependencies +="com.github.scullxbones" %% "akka-persistence-mongo-casbah" % "2.0.3"

libraryDependencies += "org.mongodb" %% "casbah" % "3.1.1"

//Test dependencies
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.3" % Test
