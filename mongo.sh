docker run -d \
    --name mongodb \
    -p 27017:27017 \
    -e MONGODB_USERNAME=test \
    -e MONGODB_PASSWORD=test \
    -e MONGODB_DBNAME=msdev frodenas/mongodb

echo "Mongo URI: mongodb://test:test@localhost:27017/msdev"
