import akka.persistence.PersistentActor

trait TestPersistency extends PersistentActor{
  override def journalPluginId: String = "akka.persistence.journal.inmem"

  override def snapshotPluginId: String = "akka.persistence.snapshot-store.local"
}
