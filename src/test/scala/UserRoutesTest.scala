import akka.http.javadsl.server.AuthorizationFailedRejection
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.atm.http.RequestModels.LoginRequestPayload
import com.atm.http.routes.{DefaultUserRoutes, RequestJsonSupport}
import com.atm.http.session.SessionHandling
import com.atm.http.session.SessionHandling.SessionData
import org.scalatest.{Matchers, WordSpec}

class UserRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with DefaultUserRoutes
  with RequestJsonSupport with SessionHandling {

  private val login = "test_routes"
  val userData: (String, String) = (login, "password")
  val sessionResponseHeader: RawHeader = sessionManager.clientSessionManager.createHeader(SessionData(login))
  val sessionRequestHeader = RawHeader("JWT", sessionResponseHeader.value)


  "Unauthenticated user service " should {

    "register user in " in {
      Post("/register",LoginRequestPayload.tupled(userData)) ~> userRoutes ~> check {
        status shouldEqual Created
      }
    }

    "be able to authenticate user with correct data" in {
      Post("/login", LoginRequestPayload.tupled(userData)) ~> userRoutes ~> check {
        status shouldEqual OK
        val tokenReceiveHeader = "token"
        val token = header(tokenReceiveHeader)

        token should not be empty

        val tokenAsString = token.get.value
        val sessionData: SessionData = sessionManager.clientSessionManager.decode(tokenAsString).toOption.get //Ugly, if crashes that means something went bad anyways

        sessionData.login shouldEqual login
      }
    }

    "reject call without token" in {
      Get("/account") ~> userRoutes ~> check {
        rejection shouldBe a [AuthorizationFailedRejection]
      }
    }

    "accept call with token" in {
      Get("/account").addHeader(sessionRequestHeader) ~> userRoutes ~> check {
        status should not equal Unauthorized
      }
    }

  }

}
