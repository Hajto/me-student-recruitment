import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.atm.domain.User
import com.atm.domain.User._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

object UserTest {
  def spawnUser(id: String)(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(Props(new User(id) with TestPersistency))
}

class UserTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  val validCredentials: (String, String) = ("test", "password")

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "An User actor" must {
    val testEntity = UserTest.spawnUser("test")

    //User creation behaviour

    "respond with error when uninitialized" in {
      testEntity ! GetBalance
      expectMsg(UserNotRegistered)
    }

    "respond success messsage to unninitialized regitration message" in {
      testEntity ! CreateUserCommand.tupled(validCredentials)
      expectMsg(UserCreated.tupled(validCredentials))
    }

    "send error when user already created" in {
      testEntity ! CreateUserCommand.tupled(validCredentials)
      expectMsg(UserAlreadyExist)
    }

    //Balance logic

    "start with balance equal to 0" in {
      testEntity ! GetBalance
      expectMsg(Balance(0))
    }

    "reject withdrawal of money when there is nothing on the account" in {
      testEntity ! WithdrawMoney(100)
      expectMsg(NotEnoughMoney)
    }

    "allow deposit money" in {
      testEntity ! DepositMoney(20)
      expectMsg(MoneyDeposited(20))
    }

    "allow to withdraw money when balance > 0" in {
      testEntity ! WithdrawMoney(10)
      expectMsg(MoneyWithdrawn(10))
    }

  }

}