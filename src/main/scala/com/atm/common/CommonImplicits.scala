package com.atm.common

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

trait CommonImplicits {
  implicit val system = ActorSystem("main-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
}
