package com.atm.http.view

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.atm.common.CommonImplicits
import com.atm.domain.User._
import com.atm.http.routes.UserAccountJsonSupport
import com.atm.http.view.UserView.{CredentialsView, ErrorView, MoneyView}

import scala.concurrent.Future


object EventCompleter extends UserAccountJsonSupport with CommonImplicits {
  def completeFutureEvent(userResult: Future[UserResult]) = userResult.map {
    case balance: Balance => complete(MoneyView(balance.value))
    case userCreated: UserCreated => complete(201, CredentialsView(userCreated.id))
    case moneyWithdrawn: MoneyWithdrawn => complete(MoneyView(moneyWithdrawn.amount))
    case moneyDeposited: MoneyDeposited => complete(MoneyView(moneyDeposited.amount))
    case UserAlreadyExist => error(409, "User already exist")
    case NotEnoughMoney => error(403, "Not enough money")
    case UserNotRegistered => error(404, "No such user")
  }

  def error(code: Int, message: String) = complete(code, ErrorView(code, message))

}
