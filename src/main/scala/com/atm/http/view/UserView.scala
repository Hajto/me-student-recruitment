package com.atm.http.view

abstract class UserView

object UserView {
  case class MoneyView(amount: Double)
  case class CredentialsView(name: String)
  case class ErrorView(code: Int, msg: String)
}
