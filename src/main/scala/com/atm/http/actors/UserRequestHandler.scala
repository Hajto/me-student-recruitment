package com.atm.http.actors

import akka.actor.{Actor, ActorLogging, Props}
import com.atm.domain.User.UserCommand
import com.atm.http.actors.UserRequestHandler.Request
import com.atm.repo.UserRegistry
import com.atm.repo.UserRegistry.QueryResult

import scala.concurrent.Promise

object UserRequestHandler {
  case class Request[T](id: String, command: UserCommand, promise : Promise[T])

  def props[T](request: Request[T]) = Props(new UserRequestHandler(request))
}

class UserRequestHandler[T](request: Request[T]) extends Actor with ActorLogging{

  override def preStart(): Unit = {
    //Ask registry for Ref of User
    UserRegistry.queryForUser(request.id)
    context.become(awaitingQueryResult)
  }

  val awaitingResult : Receive = {
    //When response comes, fulfil promise and perform gracious suicide
    case bal : T =>
      request.promise.success(bal)
      context.stop(self)
    case x => log.error(s"Received $x")
  }

  val awaitingQueryResult : Receive = {
    case QueryResult(user) =>
      user ! request.command
      context.become(awaitingResult)
  }

  override def receive: Receive = awaitingQueryResult
}
