package com.atm.http.session

import com.atm.ConfigRepo
import com.softwaremill.session.{JValueSessionSerializer, JwtSessionEncoder, SessionConfig, SessionManager}
/*
  Implicit values needed for session handler to work correctly
 */
trait SessionHandling {

  import com.atm.http.session.SessionHandling.SessionData

  val sessionConfig: SessionConfig = SessionConfig.default(ConfigRepo.sessionSecret)
  implicit val serializer = JValueSessionSerializer.caseClass[SessionData]
  implicit val encoder = new JwtSessionEncoder[SessionData]
  implicit val sessionManager = new SessionManager[SessionData](sessionConfig)
}

object SessionHandling {

  //DataModel that is being written into session JWT
  case class SessionData(login: String)

}
