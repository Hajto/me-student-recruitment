package com.atm.http.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.atm.http.RequestModels.{LoginRequestPayload, MoneyRequest}
import spray.json.DefaultJsonProtocol

/*
  Json formats for incoming requests payloads for UserRoutes
*/
trait RequestJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val loginRequestPayloadFormat = jsonFormat2(LoginRequestPayload)
  implicit val moneyRequestPayloadFormat = jsonFormat1(MoneyRequest)
}
