package com.atm.http.routes

import akka.http.scaladsl.server.Directives._
/*
  Routes definition for handling static files, including ReactApp
 */
trait StaticRoutes {
  val staticRoutes = pathEndOrSingleSlash {
    getFromResource("html/index.html")
  } ~ pathPrefix("static"){
    getFromResourceDirectory("html/static")
  }

}
