package com.atm.http.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.atm.domain.User.Balance
import com.atm.http.view.UserView.{CredentialsView, ErrorView, MoneyView}
import spray.json.DefaultJsonProtocol

/*
  Json Support for user views
*/
trait UserAccountJsonSupport extends SprayJsonSupport with DefaultJsonProtocol{
  implicit val balanceFormat = jsonFormat1(MoneyView)
  implicit val credentialsFormat = jsonFormat1(CredentialsView)
  implicit val errorView = jsonFormat2(ErrorView)
}
