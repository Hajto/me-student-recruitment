package com.atm.http.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.Credentials
import com.softwaremill.session.{SessionConfig, SessionManager}
import com.atm.common.CommonImplicits
import com.atm.domain.User._
import com.softwaremill.session.SessionDirectives._
import com.softwaremill.session.SessionOptions._
import com.atm.http.RequestModels.{LoginRequestPayload, MoneyRequest}
import com.atm.http.session.SessionHandling
import com.atm.http.session.SessionHandling.SessionData
import com.atm.http.view.EventCompleter
import com.atm.http.view.EventCompleter.error
import com.atm.repo.{UserRepo, UserRepoRoot}

import scala.concurrent.Future

trait DefaultUserRoutes {
  val userRoutes: Route = new UserRoutesRoot(UserRepo).userRoutes
}

class UserRoutesRoot(userRepo: UserRepoRoot) extends UserAccountJsonSupport with RequestJsonSupport
  with CommonImplicits with SessionHandling {
  val userRoutes: Route =
    pathPrefix("account") {
        pathEndOrSingleSlash {
          get {
            authenticated { user =>
              getResultResponse(userRepo.getBalance(user.login))
            }
          }
        } ~ path("withdraw") {
          entity(as[MoneyRequest]) { money =>
            put {
              authenticated { user =>
                getResultResponse(userRepo.withdraw(user.login, money.amount))
              }
            }
          }

        } ~ path("deposit") {
          entity(as[MoneyRequest]) { money =>
            put {
              authenticated { user =>
                getResultResponse(userRepo.deposit(user.login, money.amount))
              }
            }
          }
        }

    } ~ path("login") {
      post {
        entity(as[LoginRequestPayload]) { loginRequest =>
          onSuccess(userRepo.authenticate(loginRequest.login, loginRequest.password)) {
            case AuthenticationSuccess =>
              setSession(oneOff, usingHeaders, SessionData(loginRequest.login)) { ctx =>
                ctx.complete("Authentication successful")
              }
            case UserNotRegistered => error(404, "Wrong username or password")
            case AuthenticationFailure => error(401, "Authentication failure")
          }
        }
      }
    } ~ path("register") {
      post {
        entity(as[LoginRequestPayload]) { user =>
          getResultResponse(userRepo.register(user.login, user.password))
        }
      }
    }


  def getResultResponse(userResult: Future[UserResult]) = {
    onSuccess(EventCompleter.completeFutureEvent(userResult)) { e => e }
  }

  def authenticated(authed: SessionData => Route) = {
    requiredSession(oneOff, usingHeaders) { session => // type: Long, or whatever the T parameter is
      authed(session)
    }
  }
}
