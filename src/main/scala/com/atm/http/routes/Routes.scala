package com.atm.http.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

/**
  * Created by haito on 13.07.2017.
  */
object Routes extends DefaultUserRoutes with StaticRoutes {
  def combinedRoutes(): Route = staticRoutes ~ pathPrefix("api") {
    userRoutes
  }
}
