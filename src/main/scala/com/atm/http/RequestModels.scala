package com.atm.http

object RequestModels {
  case class LoginRequestPayload(login: String, password: String)
  case class MoneyRequest(amount: Double)
}
