package com.atm.repo

import com.atm.domain.User.{AuthenticationResult, UserResult}

import scala.concurrent.Future


trait UserRepoRoot {

  def getBalance(x: String) : Future[UserResult]
  def deposit(x: String, amount: Double) : Future[UserResult]
  def withdraw(x: String, amount: Double) : Future[UserResult]
  def authenticate(x: String, password: String) : Future[UserResult]
  def register(x: String, password: String) : Future[UserResult]

}
