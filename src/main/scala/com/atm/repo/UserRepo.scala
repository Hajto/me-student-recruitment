package com.atm.repo

import com.atm.common.CommonImplicits
import com.atm.domain.User._
import com.atm.http.actors.UserRequestHandler
import com.atm.http.actors.UserRequestHandler.Request

import scala.concurrent.{Future, Promise}

object UserRepo extends UserRepoRoot with CommonImplicits{

  def registerNewUser(name: String, password: String) = spawnActor(name, CreateUserCommand(name, password))

  def getBalance(name: String) : Future[Balance] = spawnActor(name, GetBalance)

  override def deposit(name: String, amount: Double): Future[UserResult] = spawnActor(name, DepositMoney(amount))

  override def withdraw(name: String, amount: Double): Future[UserResult] = spawnActor(name, WithdrawMoney(amount))

  override def authenticate(x: String, password: String): Future[UserResult] =
    spawnActor(x, AuthenticateUser(x, password))

  override def register(x: String, password: String): Future[UserResult] =
    spawnActor(x,CreateUserCommand(x, password))

  private def spawnActor[ExpectedResult](name: String, command: UserCommand) = promiseWrap[ExpectedResult] { promise =>
    system.actorOf(UserRequestHandler.props(Request(name, command, promise)))
  }

  //Wraps Repo Call with Promise t retun Future requested by Routing API
  private def promiseWrap[Event](sendMessage : Promise[Event] => Unit) = {
    val promise = Promise[Event]
    sendMessage(promise)
    promise.future
  }

}
