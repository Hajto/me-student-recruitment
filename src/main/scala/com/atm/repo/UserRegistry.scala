package com.atm.repo

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.atm.common.CommonImplicits
import com.atm.domain.User
import com.atm.domain.User.{CreateUserCommand, DepositMoney}


object UserRegistry extends CommonImplicits{
  abstract class UserSupervisorCommand
  case class GetOrStartChild(id: String) extends UserSupervisorCommand
  case class QueryResult(ref: ActorRef)

  val ref = system.actorOf(props, name = "user-registry")

  private def props = Props(new UserRegistry)

  def queryForUser(id: String)(implicit sender: ActorRef) = ref ! GetOrStartChild(id)
}

class UserRegistry extends Actor with ActorLogging with CommonImplicits{
  import UserRegistry.{GetOrStartChild, QueryResult}

  override def receive: Receive = {
    case GetOrStartChild(id) =>
      val user = findOrCreate(id)
      log.debug("Received a query")
      log.debug(sender().path+"")
      sender() ! QueryResult(user)
  }

  def find(id:String) : Option[ActorRef] = context child("user-"+id)
  def findOrCreate(id: String) : ActorRef = find(id) match {
    case Some(x) => x
    case None => create(id)
  }

  def create(id: String): ActorRef = context.actorOf(User.props(id), name = "user-"+id)

}