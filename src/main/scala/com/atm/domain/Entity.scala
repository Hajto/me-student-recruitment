package com.atm.domain

import akka.actor.ActorLogging
import akka.persistence.PersistentActor
import com.atm.domain.Entity.{CommandQueryNotDefined, EntityState, GetState}

/**
  * Created by haito on 13.07.2017.
  */


object Entity {
  case object GetState
  abstract class EntityState
  case object CommandQueryNotDefined
}

trait Entity extends PersistentActor with ActorLogging {

  def getState : EntityState

  def becomeWithDefault(receive: Receive) : Unit = context.become(receive orElse default)

  val default : Receive = {
    case GetState => sender ! getState
    case unknownMessage =>
      log.error(s"Illegal message received: $unknownMessage, I am: $persistenceId, My state is :$getState")
      sender() ! CommandQueryNotDefined
  }

}
