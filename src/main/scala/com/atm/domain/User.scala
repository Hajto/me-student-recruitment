package com.atm.domain

import akka.actor.Props
import akka.persistence.SnapshotOffer
import com.atm.domain.Entity.EntityState
import com.atm.domain.User._

object User {

  abstract class UserResult

  case object GetBalance extends UserCommand
  case class AuthenticateUser(name: String, password: String) extends UserCommand

  abstract class UserQueryResult extends UserResult
  case class Balance(value: Double) extends UserQueryResult
  abstract class AuthenticationResult extends UserQueryResult
  case object AuthenticationSuccess extends AuthenticationResult
  case object AuthenticationFailure extends AuthenticationResult

  abstract class UserCommand()
  case class CreateUserCommand(id: String, password: String) extends UserCommand
  case class DepositMoney(amount: Double) extends UserCommand
  case class WithdrawMoney(amount: Double) extends UserCommand

  abstract class UserEvent() extends UserResult
  case class UserCreated(id: String, password: String) extends UserEvent
  case class MoneyDeposited(amount: Double) extends UserEvent
  case class MoneyWithdrawn(amount: Double) extends UserEvent

  //Non persistent events emited out of system
  case object NotEnoughMoney extends UserEvent
  case object UserNotRegistered extends UserEvent
  case object UserAlreadyExist extends UserEvent

  case class UserState(id: String, password: String, balance: Double) extends EntityState


  def update(userEvent: UserEvent, state: UserState) : UserState = userEvent match {
    case UserCreated(x,password) => UserState(x, password, 0)
    case MoneyDeposited(amount) => state.copy(balance = state.balance+amount)
    case MoneyWithdrawn(amount) => state.copy(balance = state.balance-amount)
  }

  def props(id: String) = Props(new User(id))
}

class User(id: String) extends Entity {
  override def receiveRecover: Receive = {
    case userCreate: UserCreated =>
      becomeWithDefault(created)
      state = User.update(userCreate,state)
    case message:UserEvent =>
      state = User.update(message,state)
    case SnapshotOffer(_,stateSnap: UserState) =>
      state = stateSnap
  }

  override def receiveCommand: Receive = initialBehaviour orElse default

  override def persistenceId: String = "user-"+id

  override def getState = state

  var state : UserState = UserState("","",0) // Might want to get rid off this

  def persistEvent[A <: UserEvent](produced_event: A)(eventCallback: A => Unit) : Unit =
    persist(produced_event) { event =>
      state = User.update(event, state)
      eventCallback(event)
    }

  val created : Receive = {
    case _: CreateUserCommand => sender() ! UserAlreadyExist
    case DepositMoney(amount) => persistEvent(MoneyDeposited(amount)){event =>
      sender() ! event
    }
    case WithdrawMoney(amount) if state.balance >= amount => persistEvent(MoneyWithdrawn(amount)){event =>
      sender() ! event
    }
    case a: WithdrawMoney => sender() ! NotEnoughMoney
    case GetBalance => sender() ! Balance(state.balance)
    case AuthenticateUser(_, password) if password == state.password => sender() ! AuthenticationSuccess
    case _ : AuthenticateUser => sender() ! AuthenticationFailure
  }

  val initialBehaviour : Receive = {
    case CreateUserCommand(x,password) => persistEvent(UserCreated(x, password)) { event =>
      becomeWithDefault(created)
      sender() ! event
    }
    case a: UserCommand => sender() ! UserNotRegistered

  }
}
