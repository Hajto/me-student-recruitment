package com.atm

import com.typesafe.config.ConfigFactory

object ConfigRepo {
  private val config = ConfigFactory.load()
  private val packageName = "com.atm"

  def port = config.getInt(packageName+".http.port")
  def address = config.getString(packageName+".http.address")
  def sessionSecret = config.getString("session.secret")
}
