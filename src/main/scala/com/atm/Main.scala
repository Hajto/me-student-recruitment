package com.atm

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import com.atm.common.CommonImplicits
import com.atm.http.routes.Routes

/**
  * Created by haito on 13.07.2017.
  */
object Main extends CommonImplicits{
  def main(args: Array[String]): Unit = {
    implicit val executionContext = system.dispatcher
    val routes: Route = Routes.combinedRoutes()

    val bindingFuture = Http().bindAndHandle(routes, ConfigRepo.address, ConfigRepo.port)
  }
}
